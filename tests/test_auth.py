
### Test cases:

# User gets the login page rendered

# User gets logged in successfully 

# User gets login denied

# When logged out, user cannot access any of the authorized endpoints

# When logged under a user with read-only privileges, user cannot access the read-write endpoints and can access the rest

# When logged under a user with read-write privileges, user can access all endpoints