
### Test cases:

# User gets redirected from / to /visits/

# User gets the visit listing page rendered

# User gets the visit creation page rendered

# User gets the visit edit page rendered

# User successfully creates a visit

# User successfully edits a visit

# User successfully deletes a visit

# User gets a 400 on invalid input data

# User gets a 404 a on non-existent item