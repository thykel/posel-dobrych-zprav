
### Test cases:

# User gets the shop listing page rendered

# User gets the shop creation page rendered

# User gets the shop edit page rendered

# User successfully creates a shop

# User successfully edits a shop

# User successfully deletes a shop

# User gets a 400 on invalid input data

# User gets a 404 a on non-existent item