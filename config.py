import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'blahbleh'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'postgresql://posel:atol318@localhost/posel'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
