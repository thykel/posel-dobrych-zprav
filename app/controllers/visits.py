from app import app, db
from flask import render_template, redirect, url_for, jsonify, request
from flask_login import current_user, login_required
from app.models import Visit, VisitSchema, Shop
from app.forms import AddVisitForm, EditVisitForm
from datetime import datetime, timedelta

### Page retrieval queries


@app.route("/")
@app.route("/visits", methods=["GET"])
@login_required
def visits():
    shop_id = request.args.get("shop_id")
    show_all = request.args.get("show_all")
    try:
        shop_name = Shop.query.get(shop_id).name if shop_id else None
    except:
        shop_id = None
        shop_name = None

    return render_template(
        "visits/visits.html",
        shop_id=shop_id,
        shop_name=shop_name,
        user_id=current_user.id if not show_all else None,
        user_name=current_user.username if not show_all else None,
        editable=current_user.has_write_access,
    )


@app.route("/visits/add", methods=["GET"])
@login_required
def visit_add_get():
    form = AddVisitForm()
    form.shop_id.choices = [(s.id, s.name) for s in Shop.query.order_by("name")]
    return render_template("visits/visit_add.html", form=form)


@app.route("/visits/edit/<id>", methods=["GET"])
@login_required
def visit_edit_get(id):
    now = datetime.now()
    visit = Visit.query.get(id)

    form = EditVisitForm()

    form.shop_id.choices = [(s.id, s.name) for s in Shop.query.order_by("name")]
    form.shop_id.data = visit.shop_id
    form.plan.data = visit.plan
    form.result.data = visit.result
    form.was_successful.data = visit.was_successful
    form.time.data = visit.time

    return render_template(
        "visits/visit_edit.html",
        form=form,
        include_result=True if visit.time < now else False,
    )


### Data manipulation queries


@app.route("/past_visits_data")
@login_required
def past_visits_data(shop_id=None):
    now = datetime.now()

    shop_id = request.args.get("shop_id")
    user_id = request.args.get("user_id")

    visit_schema = VisitSchema()
    visits = Visit.query.filter(Visit.time < now).order_by(Visit.time.desc())
    if shop_id:
        visits = visits.filter(Visit.shop_id == shop_id)

    if user_id:
        visits = visits.join(Visit.shop, Shop.managing_user).filter(Shop.managing_user_id == user_id)

    serialized_visits = visit_schema.dump(visits.all(), many=True)

    past_visits = list(filter(lambda v: v["time"], serialized_visits))
    return jsonify({"data": past_visits})


@app.route("/next_visits_data")
@login_required
def next_visits_data():
    now = datetime.now()

    shop_id = request.args.get("shop_id")
    user_id = request.args.get("user_id")

    visit_schema = VisitSchema()
    visits = Visit.query.filter(Visit.time > now).order_by(Visit.time.asc())

    if shop_id:
        visits = visits.filter(Visit.shop_id == shop_id)

    if user_id:
        visits = visits.join(Visit.shop, Shop.managing_user).filter(Shop.managing_user_id == user_id)

    serialized_visits = visit_schema.dump(visits.all(), many=True)

    next_visits = list(filter(lambda v: v["time"], serialized_visits))
    return jsonify({"data": next_visits})


@app.route("/visits/add", methods=["POST"])
@login_required
def visit_add_post():
    form = AddVisitForm()
    form.shop_id.choices = [(s.id, s.name) for s in Shop.query.order_by("name")]

    if form.validate_on_submit():
        visit = Visit()
        visit.shop_id = form.shop_id.data
        visit.time = form.time.data
        visit.plan = form.plan.data

        db.session.add(visit)
        db.session.commit()
        return redirect(url_for("visits"))
    else:
        return "Invalid parameters! " + repr(form.errors), 400


@app.route("/visits/edit/<id>", methods=["POST"])
@login_required
def visit_edit_post(id):
    visit = Visit.query.get(id)
    form = EditVisitForm()
    form.shop_id.choices = [(s.id, s.name) for s in Shop.query.order_by("name")]

    if form.validate_on_submit():
        visit.shop_id = form.shop_id.data
        visit.plan = form.plan.data
        visit.result = form.result.data
        visit.was_successful = form.was_successful.data
        visit.time = form.time.data

        db.session.add(visit)
        db.session.commit()
        return redirect(url_for("visits"))

    else:
        return "Invalid parameters! " + repr(form.errors), 400


@app.route("/visits/delete/<id>", methods=["DELETE"])
@login_required
def visit_delete(id):
    v = Visit.query.get(id)
    db.session.delete(v)
    db.session.commit()
    return "", 200
