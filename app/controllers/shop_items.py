from flask import render_template, redirect, url_for, jsonify
from flask_login import current_user, login_required
from app import app, db
from app.forms import AddShopItemForm, EditShopItemForm
from app.models import ShopItem, ShopItemSchema

### Page retrieval queries


@app.route("/shop_items")
@login_required
def shop_items():
    return render_template(
        "shop_items/shop_items.html", editable=current_user.has_write_access
    )


@app.route("/shop_items/add", methods=["GET"])
@login_required
def shop_item_add_get():
    return render_template("shop_items/shop_item_add.html", form=AddShopItemForm())


@app.route("/shop_items/edit/<id>", methods=["GET"])
@login_required
def shop_item_edit_get(id):
    shop_item = ShopItem.query.get(id)

    form = EditShopItemForm()
    form.name.data = (
        shop_item.name
    )  # Replace the ID of the linked shop with its full name

    return render_template("shop_items/shop_item_edit.html", form=form)


### Data manipulation queries
@app.route("/shop_items_data")
@login_required
def shops_items_data():
    shop_item_schema = ShopItemSchema()
    shop_items = ShopItem.query.all()
    return jsonify({"data": shop_item_schema.dump(shop_items, many=True)})


@app.route("/shop_items/add", methods=["POST"])
@login_required
def shop_item_add_post():
    form = AddShopItemForm()

    if form.validate_on_submit():
        # Parse the list of items out of the newline-separated list
        for i in set(form.name.data.split("\n")):
            db.session.add(ShopItem(name=i))

        db.session.commit()
        return redirect(url_for("shop_items"))
    else:
        return "Invalid parameters!", 400


@app.route("/shop_items/edit/<id>", methods=["POST"])
@login_required
def shop_item_edit_post(id):
    form = EditShopItemForm()
    shop_item = ShopItem.query.get(id)

    if form.validate_on_submit():
        shop_item.name = form.name.data
        db.session.add(shop_item)
        db.session.commit()
        return redirect(url_for("shop_items"))
    else:
        return "Invalid parameters!", 400


@app.route("/shop_items/delete/<id>", methods=["DELETE"])
@login_required
def shop_item_delete(id):
    si = ShopItem.query.get(id)
    si.shops = []
    db.session.delete(si)
    db.session.commit()
    return "", 200
