from flask import render_template, redirect, url_for, jsonify, make_response
from flask_login import current_user, login_required
from app import app, db
from app.forms import AddShopForm, EditShopForm
from app.models import Shop, ShopItem, ShopProperty, ShopSchema, User
from re import findall
### Page retrieval queries


@app.route("/shops")
@login_required
def shops():
    return render_template("shops/shops.html", editable=current_user.has_write_access)


@app.route("/shops/add", methods=["GET"])
@login_required
def shop_add_get():
    form = AddShopForm()
    form.managing_user.choices = [
        (u.id, u.username) for u in User.query.order_by("username")
    ]
    form.sortiment.choices = [(s.id, s.name) for s in ShopItem.query.order_by("name")]
    form.properties.choices = [
        (s.id, s.name) for s in ShopProperty.query.order_by("name")
    ]

    return render_template("shops/shop_add.html", form=form)


@app.route("/shops/edit/<id>", methods=["GET"])
@login_required
def shop_edit_get(id):
    shop = Shop.query.get(id)
    form = EditShopForm()

    form.managing_user.choices = [
        (u.id, u.username) for u in User.query.order_by("username")
    ]
    form.sortiment.choices = [(s.id, s.name) for s in ShopItem.query.order_by("name")]
    form.properties.choices = [
        (s.id, s.name) for s in ShopProperty.query.order_by("name")
    ]
    checked_sortiment = [s.id for s in shop.sortiment]
    checked_properties = [s.id for s in shop.properties]

    form.name.data = shop.name
    form.is_active.data = shop.is_active
    form.address.data = shop.address
    form.number.data = shop.number
    form.hours.data = shop.hours
    form.source_shop.data = shop.source_shop
    form.managing_user.data = shop.managing_user_id

    return render_template(
        "shops/shop_edit.html",
        form=form,
        sortiment=checked_sortiment,
        properties=checked_properties,
    )


### Data manipulation queries


@app.route("/shops_data")
@login_required
def shops_data():
    shop_schema = ShopSchema()
    shops = Shop.query.all()
    return jsonify(
        {
            "data": shop_schema.dump(shops, many=True),
            "shop_items": [si.name.strip() for si in ShopItem.query.all()],
            "shop_properties": [sp.name.strip() for sp in ShopProperty.query.all()],
            "managing_user_names": [u.username.strip() for u in User.query.all()],
        }
    )


@app.route("/shops/add", methods=["POST"])
@login_required
def shop_add_post():
    form = AddShopForm()
    form.sortiment.choices = [(s.id, s.name) for s in ShopItem.query.order_by("name")]
    form.properties.choices = [
        (s.id, s.name) for s in ShopProperty.query.order_by("name")
    ]

    # FIXME: Find out how to execute form validation on checkbox arrays
    # if form.validate_on_submit():
    if True:
        # Get data from form and assign it to the correct attributes
        # of the SQLAlchemy table object
        shop = Shop()
        shop.name = form.name.data
        shop.is_active = form.is_active.data
        shop.address = form.address.data
        shop.number = form.number.data
        shop.hours = form.hours.data
        shop.source_shop = form.source_shop.data
        shop.managing_user_id = form.managing_user.data

        items = ShopItem.query.filter(ShopItem.id.in_(form.sortiment.data)).all()
        shop.sortiment = items

        props = ShopProperty.query.filter(
            ShopProperty.id.in_(form.properties.data)
        ).all()
        shop.properties = props

        db.session.add(shop)
        db.session.commit()
        return redirect(url_for("shops"))
    else:
        return "Invalid parameters! " + repr(form.errors), 400


@app.route("/shops/edit/<id>", methods=["POST"])
@login_required
def shop_edit_post(id):
    form = EditShopForm()
    form.sortiment.choices = [(s.id, s.name) for s in ShopItem.query.order_by("name")]
    form.properties.choices = [
        (s.id, s.name) for s in ShopProperty.query.order_by("name")
    ]

    shop = Shop.query.get(id)

    # FIXME: Find out how to execute form validation on checkbox arrays
    # if form.validate_on_submit():
    if True:
        shop.name = form.name.data
        shop.is_active = form.is_active.data
        shop.number = form.number.data
        shop.address = form.address.data
        shop.hours = form.hours.data
        shop.source_shop = form.source_shop.data
        shop.managing_user_id = form.managing_user.data

        items = ShopItem.query.filter(ShopItem.id.in_(form.sortiment.data)).all()
        shop.sortiment = items

        props = ShopProperty.query.filter(
            ShopProperty.id.in_(form.properties.data)
        ).all()
        shop.properties = props

        db.session.add(shop)
        db.session.commit()
        return redirect(url_for("shops"))
    else:
        return "Invalid parameters! " + repr(form.errors), 400


@app.route("/shops/delete/<id>", methods=["DELETE"])
@login_required
def shop_delete(id):
    sh = Shop.query.get(id)
    sh.visits = []
    sh.sortiment.clear()
    db.session.delete(sh)
    db.session.commit()
    return "", 200

@app.route("/shops/export_emails", methods=["GET"])
@login_required
def export_emails():
    contact_data = ''.join(
        [''.join(s) for s in Shop.query.with_entities(Shop.number).all()]
    )

    emails = set(findall(r"[a-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+", contact_data))

    response = make_response("\n".join(emails))
    response.headers["Content-Disposition"] = "attachment; filename=e-mails.txt"

    return response


