from flask import render_template, redirect, url_for, jsonify
from flask_login import current_user, login_required
import flask_restful
from app import app, db, api
from app.forms import AddShopPropertyForm, EditShopPropertyForm
from app.models import ShopProperty, ShopPropertySchema



### Page retrieval queries

@app.route("/shop_properties")
@login_required
def shop_properties():
    return render_template(
        "shop_properties/shop_properties.html", editable=current_user.has_write_access
    )


@app.route("/shop_properties/add", methods=["GET"])
@login_required
def shop_property_add_get():
    return render_template(
        "shop_properties/shop_property_add.html", form=AddShopPropertyForm()
    )


@app.route("/shop_properties/edit/<id>", methods=["GET"])
@login_required
def shop_property_edit_get(id):
    shop_item = ShopProperty.query.get(id)

    form = EditShopPropertyForm()
    form.name.data = (
        shop_item.name
    )  # Replace the ID of the linked shop with its full name

    return render_template("shop_properties/shop_property_edit.html", form=form)




class ShopPropertyListResource(flask_restful.Resource):
    # method_decorators = [login_required]

    def get(self):
        shop_properties_schema = ShopPropertySchema()
        shop_properties = ShopProperty.query.all()
        return jsonify({"data": shop_properties_schema.dump(shop_properties, many=True)})


    def post(self):
        form = AddShopPropertyForm()

        if form.validate_on_submit():
            # Parse the list of items out of the newline-separated list
            for i in set(form.name.data.split("\n")):
                db.session.add(ShopProperty(name=i))

            db.session.commit()
            return redirect(url_for("shop_properties"))
        else:
            return "Invalid parameters!", 400


class ShopPropertyResource(flask_restful.Resource):
    # method_decorators = [login_required]

    def get(self, property_id):
        shop_properties_schema = ShopPropertySchema()
        shop_property = ShopProperty.query.get(property_id)
        return jsonify({"data": shop_properties_schema.dump(shop_property)})


    def put(self, property_id):
        form = EditShopPropertyForm()
        shop_item = ShopProperty.query.get(property_id)

        if form.validate_on_submit():
            shop_item.name = form.name.data
            db.session.add(shop_item)
            db.session.commit()
            return redirect(url_for("shop_properties"))
        else:
            return "Invalid parameters!", 400

    def delete(self, property_id):
        si = ShopProperty.query.get(id)
        si.shops = []
        db.session.delete(si)
        db.session.commit()
        return "", 200        

api.add_resource(ShopPropertyListResource, '/api/shop_properties')
api.add_resource(ShopPropertyResource, '/api/shop_properties/<int:property_id>')