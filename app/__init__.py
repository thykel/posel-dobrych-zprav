from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_bootstrap import Bootstrap
from flask_debugtoolbar import DebugToolbarExtension
from flask_restful import Api

app = Flask(__name__)
app.config.from_object(Config)

api = Api(app)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
bootstrap = Bootstrap(app)
ma = Marshmallow(app)

# Uncomment below for advanced debugging
# toolbar = DebugToolbarExtension(app)
# app.debug = True
app.config["TEMPLATES_AUTO_RELOAD"] = True
# app.config['SQLALCHEMY_RECORD_QUERIES'] = True
# app.config['DEBUG_TB_PROFILER_ENABLED'] = True
# app.config['PROPAGATE_EXCEPTIONS'] = True

login_manager = LoginManager(app)
login_manager.login_view = "login"
login_manager.login_message = "Pro vstup do aplikace je třeba se přihlásit."

from app import models
from app.controllers import auth, visits, shops, shop_items, shop_properties
