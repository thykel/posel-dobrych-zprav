from flask_wtf import FlaskForm
from wtforms import (
    StringField,
    PasswordField,
    BooleanField,
    SubmitField,
    SelectField,
    SelectMultipleField,
    TextAreaField,
    DateTimeField,
)
from wtforms import widgets
from wtforms.validators import DataRequired
from datetime import datetime


class LoginForm(FlaskForm):
    username = StringField("Uživatel", validators=[DataRequired()])
    password = PasswordField("Heslo", validators=[DataRequired()])
    remember_me = BooleanField("Zapamatovat")
    submit = SubmitField("Přihlásit se")


class PamlskyForm(SelectMultipleField):
    widget = widgets.ListWidget(prefix_label=False)
    option_widget = widgets.CheckboxInput()


class VybaveniForm(SelectMultipleField):
    widget = widgets.ListWidget(prefix_label=False)
    option_widget = widgets.CheckboxInput()


class AddShopForm(FlaskForm):
    name = StringField("Název", validators=[DataRequired()])
    is_active = BooleanField("Aktivní", default=True)
    address = TextAreaField("Adresa", validators=[DataRequired()])
    number = TextAreaField("Kontakt", validators=[DataRequired()])
    hours = TextAreaField("Hodiny", validators=[DataRequired()])
    source_shop = StringField("Velkoobchod")
    managing_user = SelectField("Obsluha", coerce=int)

    sortiment = PamlskyForm("Sortiment", choices=[])
    properties = VybaveniForm("Vybavení", choices=[])

    submit = SubmitField("Přidat obchod")


class EditShopForm(FlaskForm):
    name = StringField("Název", validators=[DataRequired()])
    is_active = BooleanField("Aktivní")
    address = TextAreaField("Adresa", validators=[DataRequired()])
    number = TextAreaField("Kontakt", validators=[DataRequired()])
    hours = TextAreaField("Hodiny", validators=[DataRequired()])
    source_shop = StringField("Velkoobchod")
    managing_user = SelectField("Obsluha", coerce=int)

    sortiment = PamlskyForm("Sortiment", choices=[])
    properties = VybaveniForm("Vybavení", choices=[])

    submit = SubmitField("Upravit obchod")


class AddVisitForm(FlaskForm):
    shop_id = SelectField("Obchod", coerce=int)
    time = DateTimeField(
        "Čas návštěvy",
        validators=[DataRequired()],
        format="%Y-%m-%d %H:%M",
        default=datetime.now,
    )
    plan = TextAreaField("Plán", validators=[DataRequired()])
    submit = SubmitField("Přidat návštěvu")


class EditVisitForm(FlaskForm):
    shop_id = SelectField("Obchod", coerce=int)
    time = DateTimeField(
        "Čas návštěvy", validators=[DataRequired()], format="%Y-%m-%d %H:%M"
    )
    plan = TextAreaField("Plán", validators=[DataRequired()])
    result = TextAreaField("Hodnocení")
    was_successful = BooleanField("Byla návštěva úspěšná?", default=True)
    submit = SubmitField("Upravit návštěvu")


class AddShopItemForm(FlaskForm):
    name = TextAreaField(
        "Položky (jeden produkt na řádek)", validators=[DataRequired()]
    )
    submit = SubmitField("Přidat položky")


class EditShopItemForm(FlaskForm):
    name = StringField("Jméno", validators=[DataRequired()])
    submit = SubmitField("Upravit položku")


class AddShopPropertyForm(FlaskForm):
    name = TextAreaField(
        "Vybavení (jeden produkt na řádek)", validators=[DataRequired()]
    )
    submit = SubmitField("Přidat vybavení")


class EditShopPropertyForm(FlaskForm):
    name = StringField("Jméno", validators=[DataRequired()])
    submit = SubmitField("Upravit položku")
