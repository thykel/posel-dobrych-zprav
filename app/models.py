from app import db, login_manager, ma
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from marshmallow import fields

### DB models


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    has_write_access = db.Column(db.Boolean)
    shops = db.relationship("Shop", backref="managing_user", lazy=True)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return "<User {}>".format(self.username)


@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


# M:N relation table for shop and shop_item
shop_sortiment = db.Table(
    "shop_sortiment",
    db.Column("shop_id", db.Integer, db.ForeignKey("shop.id"), primary_key=True),
    db.Column(
        "shop_item_id", db.Integer, db.ForeignKey("shop_item.id"), primary_key=True
    ),
)

# M:N relation table for shop and shop_peroperty
shop_properties = db.Table(
    "shop_properties",
    db.Column("shop_id", db.Integer, db.ForeignKey("shop.id"), primary_key=True),
    db.Column(
        "shop_property_id",
        db.Integer,
        db.ForeignKey("shop_property.id"),
        primary_key=True,
    ),
)


class ShopProperty(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True)


class ShopItem(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True)


class Shop(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    address = db.Column(db.String)
    number = db.Column(db.String)
    hours = db.Column(db.String)
    source_shop = db.Column(db.String)
    managing_user_id = db.Column(db.Integer, db.ForeignKey("user.id"))

    is_active = db.Column(db.Boolean)

    visits = db.relationship("Visit", backref="shop", lazy=True)
    sortiment = db.relationship(
        "ShopItem",
        secondary=shop_sortiment,
        lazy="joined",
        backref=db.backref("shops", lazy=True),
    )
    properties = db.relationship(
        "ShopProperty",
        secondary=shop_properties,
        lazy="joined",
        backref=db.backref("shops", lazy=True),
    )

    def get_last_visit(self):
        # TODO: This is mostly going to be processing just few items each time, but it is still needlessly inefficient,
        # Let's delegate this query to SQLAlchemy.
        sorted_visits = sorted(self.visits, key=lambda v: v.time)
        if sorted_visits:
            return sorted_visits[-1]
        else:
            return None


class Visit(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    time = db.Column(db.DateTime)
    plan = db.Column(db.String)
    result = db.Column(db.String)
    was_successful = db.Column(db.Boolean)

    shop_id = db.Column(db.Integer, db.ForeignKey("shop.id"), nullable=False)


### Schemas


class UserSchema(ma.SQLAlchemySchema):
    class Meta:
        model = User

    id = ma.auto_field()
    username = ma.auto_field()


class ShopSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Shop

    id = ma.auto_field()
    name = ma.auto_field()
    address = ma.auto_field()
    number = ma.auto_field()
    hours = ma.auto_field()
    is_active = ma.auto_field()
    source_shop = ma.auto_field()
    managing_user = fields.Nested(UserSchema(only=("id", "username")))

    source_shop = fields.Method("get_source_shop")
    last_visit_time = fields.Method("get_last_visit_time")
    last_visit_success = fields.Method("was_last_visit_successful")
    sortiment = fields.Method("get_sortiment_names")
    properties = fields.Method("get_property_names")

    def get_source_shop(self, shop):
        return shop.source_shop

    def get_property_names(self, shop):
        return sorted([s.name for s in shop.properties])

    def get_sortiment_names(self, shop):
        return sorted([s.name for s in shop.sortiment])

    def get_last_visit_time(self, shop):
        last_visit = shop.get_last_visit()
        return last_visit.time if last_visit else False

    def was_last_visit_successful(self, shop):
        last_visit = shop.get_last_visit()
        return last_visit.was_successful if last_visit else False


class VisitSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Visit

    id = ma.auto_field()
    time = ma.auto_field()
    plan = ma.auto_field()
    result = ma.auto_field()
    was_successful = ma.auto_field()
    shop = fields.Nested(
        ShopSchema(only=("id", "name", "address", "source_shop", "managing_user"))
    )


class ShopItemSchema(ma.SQLAlchemySchema):
    class Meta:
        model = ShopItem

    id = ma.auto_field()
    name = ma.auto_field()


class ShopPropertySchema(ma.SQLAlchemySchema):
    class Meta:
        model = ShopProperty

    id = ma.auto_field()
    name = ma.auto_field()
